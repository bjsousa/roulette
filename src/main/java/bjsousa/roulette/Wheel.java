package bjsousa.roulette;

import java.util.ArrayList;

public class Wheel {

	private ArrayList<Bin> bins;
	
	public Wheel() {
		this.bins = new ArrayList<Bin>();	
		for (int i=0; i<38; i++) {
			this.bins.add(new Bin());
		}
		
	}
	
/**
 * Adds the given {@link Outcome} to the {@link Bin} with the given number.
 * @param index index of Bin
 * @param outcome Outcome to add
 */
	void addOutcome(int index, Outcome outcome){

		this.get(index).add(outcome);
		
	}
	
/**
 * Returns the given {@link Bin} from the internal collection.
 * @param index  index of Bin
 * @return  Bin at given index
 */
	public Bin get(int index) {
		
		return this.bins.get(index);
		
	}
	void setBin(int index, Bin bin){
		
		this.bins.set(index,bin);
		
	}
	
	 void setBins(ArrayList<Bin> bins){
		
		this.bins = bins;
		
	}

	 ArrayList<Bin> getBins() {
		
		return this.bins;
		
	}
}
