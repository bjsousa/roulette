package bjsousa.roulette;

import java.text.MessageFormat;

/**
 * Defines one possible outcome that players can bet on for each spin of the
 * roulette wheel.
 * 
 * @author sousa
 *
 */
public class Outcome implements Comparable<Outcome> {

	private String name;
	private int odds;

	/**
	 * Constructor for class
	 * 
	 * @param name
	 *            name of outcome
	 * @param odds
	 *            odds of winning in format <odds>:1
	 */
	public Outcome(String name, int odds) {
		this.name = name;
		this.odds = odds;
	}

	/**
	 * Calculates the winnings for an outcome by multiplying amount of bet by
	 * odds of Outcome.
	 * 
	 * @param Integer
	 *            for amount of bet
	 * @return Integer for amount of winnings
	 */
	public int winAmount(int amount) {

		return amount * odds;

	}

	/**
	 * Ensures that comparison of {@link Outcome} objects will be tested by
	 * comparing String objects returned by .getName method
	 * 
	 * @param other
	 *            object
	 * @return true if Outcomes names are equal
	 */
	@Override
	public boolean equals(Object other) {
		Outcome test = (Outcome) other;

		return test.getName().equals(name);
	}

	/**
	 * Returns object represented as a string in format: [name] ([odds]:1)
	 * 
	 * @return String showing name and odds attributes
	 */
	@Override
	public String toString() {
		Object[] values = { name, odds };
		String msgTempl = "{0} ({1}:1)";
		return MessageFormat.format(msgTempl, values);
	}

	/**
	 * Override compareTo method to use name attribute
	 * 
	 * @param Outcome
	 *            object
	 * @return Boolean
	 */
	@Override
	public int compareTo(Outcome o) {

		return this.name.compareTo(o.name);

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getOdds() {
		return odds;
	}

	public void setOdds(int odds) {
		this.odds = odds;
	}

}
