package bjsousa.roulette;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

/**
 * Defines a bin on the roulette wheel and the set of Outcomes associated with
 * it.
 * 
 * @author sousa
 *
 */
public class Bin {

	private Set<Outcome> outcomes;

	/**
	 * Constructor for class
	 * 
	 * create empty object
	 */
	public Bin() {
		this.outcomes = new TreeSet<>();
	}

	/**
	 * Constructor for class
	 * 
	 * @param array
	 *            of Outcomes
	 */
	public Bin(Outcome[] array) {

		this();
		for (int i = 0; i < array.length; i++) {
			this.add(array[i]);
		}

	}

	/**
	 * Constructor for class
	 * 
	 * @param collection
	 *            of Outcomes
	 */
	public Bin(Collection<Outcome> outcomes) {

		this.outcomes = new TreeSet<>(outcomes);

	}

	/**
	 * Add method Adds single Outcome to Bin
	 * 
	 * @param Outcome
	 *            object
	 */
	public void add(Outcome out) {
		this.outcomes.add(out);
	}

	/**
	 * Override toString method to read: [name] ([odds]:1)
	 * 
	 * @param Bin
	 *            object
	 * @return String listing Outcomes in Bin
	 */
	@Override
	public String toString() {

		return this.outcomes.toString();

	}

	void setOutcomes(TreeSet<Outcome> outcomes) {
		this.outcomes = outcomes;
	}

	public Set<Outcome> getOutcomes() {
		return this.outcomes;
	}

}
