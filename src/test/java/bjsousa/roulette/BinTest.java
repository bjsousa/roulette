package bjsousa.roulette;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests for the Bin class.
 * 
 * @author sousa
 *
 */
public class BinTest {

	Outcome five;
	TreeSet<Outcome> outcomes;

	Bin binempty;
	Bin binset;

	@Before
	public void before() {

		five = new Outcome("00-0-1-2-3", 6);
		outcomes = new TreeSet<Outcome>();
		outcomes.add(new Outcome("00", 35));
		outcomes.add(new Outcome("0", 35));
		outcomes.add(five);

		binempty = new Bin();
		binset = new Bin();
		binset.setOutcomes(outcomes);

	}

	/**
	 * Verifies that {@link Bin#toString(Object)} outputs object information in
	 * correct string format.
	 */
	@Test
	public void tostring_control() {

		assertEquals("[0 (35:1), 00 (35:1), 00-0-1-2-3 (6:1)]",
				binset.toString());

	}

	/**
	 * Verifies that a Bin object with no outcomes added returns True when
	 * {@link Bin#getOutcomes(Object)} is checked for an empty collection.
	 */
	@Test
	public void bin_empty_control() {
		Bin empty = new Bin();
		assertTrue(empty.getOutcomes().isEmpty());

	}

	/**
	 * Verifies that {@link Bin#add(Object)} adds an outcome to the collection
	 * in a Bin object.
	 */
	@Test
	public void add_control() {
		Bin addtest = new Bin();
		addtest.add(five);
		assertTrue(addtest.getOutcomes().contains(five));

	}

	/**
	 * Verifies that the array-based Bin constructor adds an array of objects
	 * correctly as the Bin's Outcomes.
	 */
	@Test
	public void array_constructor_control() {

		Outcome[] primitive = outcomes.toArray(new Outcome[outcomes.size()]);

		Bin bin = new Bin(primitive);

		assertEquals(outcomes, bin.getOutcomes());
	}

}
