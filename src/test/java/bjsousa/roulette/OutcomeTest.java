package bjsousa.roulette;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Tests for the Outcome class.
 * 
 * @author sousa
 *
 */
public class OutcomeTest {

	/**
	 * Verifies that {@link Outcome#equals(Object)} returns true when Outcomes
	 * are the same.
	 */
	@Test
	public void equals_returns_true_when_equal() {
		Outcome testOutcome1 = new Outcome("red", 1);
		Outcome testOutcome2 = new Outcome("red", 1);

		assertTrue(testOutcome1.equals(testOutcome2));

	}

	/**
	 * Verifies that {@link Outcome#equals(Object)} returns false when Outcomes
	 * are not the same.
	 */
	@Test
	public void equals_returns_false_when_unequal() {
		Outcome testOutcome1 = new Outcome("red", 1);
		Outcome testOutcome2 = new Outcome("split", 17);

		assertFalse(testOutcome1.equals(testOutcome2));

	}

	/**
	 * Verifies that {@link Outcome#toString()} outputs object information in
	 * correct string format.
	 * 
	 */
	@Test
	public void toString_control() {
		Outcome testOutcome = new Outcome("split", 17);

		assertTrue(testOutcome.toString().equals("split (17:1)"));

	}

	/**
	 * Verifies that {@link Outcome#winAmmount()} returns correct value.
	 * 
	 */
	@Test
	public void winAmount_control() {

		assertEquals(17 * 2, new Outcome("split", 17).winAmount(2));

	}

}
