package bjsousa.roulette;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

public class WheelTest {

	/*
	 * Verifies that Wheel constructor creates 
	 * object that contains a vector of 38 bins.
	 */
	@Test
	public void constructor_control() {
		
		Wheel wheel = new Wheel();
		
		assertEquals(38,wheel.getBins().size());
		
	}
	
	/*
	 * Verifies that the {@link Wheel#get(Object)}
	 *  returns the outcomes for the correct bin.
	 * 
	 */
	@Test
	public void get_control() {
		Wheel wheel = new Wheel();
		ArrayList<Bin> testbins = new ArrayList<Bin>();
		Bin bin = new Bin();
		
		testbins.add(0,bin);
		wheel.setBins(testbins);
		
		assertEquals(bin,wheel.get(0));
		
	}
	
	/*
	 * Verify that {@link Wheel#addOutcome(Object)}
	 * adds an outcome to the selected bin.
	 */
	@Test
	public void addOutcome_control() {
		Wheel wheel = new Wheel();
		Outcome outcome = new Outcome("0",35);
		wheel.addOutcome(0,outcome); 
		assertTrue(wheel.getBins().get(0).getOutcomes().contains(outcome));
	}
	
}
